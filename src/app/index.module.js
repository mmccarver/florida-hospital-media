(function() {
  'use strict';

  angular
    .module('media', ['ngAnimate', 'restangular', 'ui.router', 'ui.bootstrap', 'toastr']);

})();
