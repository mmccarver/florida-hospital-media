'use strict'

angular.module('media')

.provider('apiUrl', function(){

  this.url = function(){
    if(window.location.href.indexOf('local') > -1){

      console.log('dev url');
      return 'https://dev-api.rbmtv.com/v5/int/';

    }else{
      console.log('prod url');
      return 'https://api.rbmtv.com/v5/int/';
    }
  }

  //provider always needs a $get method
  this.$get = function(){};
})
