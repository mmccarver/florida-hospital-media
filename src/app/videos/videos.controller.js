(function() {
  'use strict';

  angular
    .module('media')
    .controller('VideosController', VideosController);

  /** @ngInject */
  function VideosController($scope, Restangular) {
    console.log('VideosController!');
    function init(){

      Restangular.one('encoded_assets').get()
        .then(function(data){
          var data = data.plain();
          // $scope.videos = data.response.assets;
          $scope.videos = _.values(data.response.assets);

          console.log($scope.videos);
        }, function(error){

        })

    }

    init();
  }
})();
