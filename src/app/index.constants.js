/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('media')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
