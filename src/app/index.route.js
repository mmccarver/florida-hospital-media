(function() {
  'use strict';

  angular
    .module('media')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController'
      })

      .state('playlists',{
        url:'/playlists',
        templateUrl: 'app/playlists/playlists.html',
        controller: 'PlaylistsController'
      })

      .state('videos',{
        url:'/videos',
        templateUrl:'app/videos/videos.html',
        controller:'VideosController'
      })

    $urlRouterProvider.otherwise('/');
  }

})();
