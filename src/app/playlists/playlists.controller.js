(function() {
  'use strict';

  angular
    .module('media')
    .controller('PlaylistsController', PlaylistsController);

  /** @ngInject */
  function PlaylistsController($scope, Restangular) {
    console.log('playlists!');

    function init(){

      Restangular.one('playlists').get()
        .then(function(data){
          var data = data.plain();

          $scope.playlists = _.values(data.response.playlists);

          console.log($scope.playlists);
        }, function(error){

        });
    }

    init();
  }
})();
