(function() {
  'use strict';

  angular
    .module('media')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, apiUrlProvider, RestangularProvider) {

    RestangularProvider.setDefaultRequestParams({token: '48ad685c3471fe403bb8e68cb3dced88'});

    RestangularProvider.setBaseUrl(apiUrlProvider.url());

  }

})();
